package za.co.joshuagoodman.dottxtcounter;

public class TxtFileUtil {

    public static final String TXT_FILE_EXT = ".txt";

    public static Boolean isTextFile(String path) {
        try {
            if (path.substring(path.length() - TXT_FILE_EXT.length()).toLowerCase().equals(TXT_FILE_EXT))
                return true;
        } catch (Exception ignored) {
        }
        return false;
    }
}

package za.co.joshuagoodman.dottxtcounter;

public class TextFileInputModel {
    public String path;
    public Integer charCount;

    public TextFileInputModel(Integer charCount, String path) {
        this.charCount = charCount;
        this.path = path;
    }
}

package za.co.joshuagoodman.dottxtcounter;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.NumberPicker;
import android.widget.Toast;

import com.software.shell.fab.ActionButton;

public class MainActivity extends ActionBarActivity {

    public static final Integer DEFAULT_CHAR_COUNT = 7;
    public static final Integer DEFAULT_CHAR_COUNT_MAX = 14;
    public static final Integer DEFAULT_CHAR_COUNT_MIN = 1;

    ActionButton actionButton;
    NumberPicker numberPicker;
    Integer charCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        actionButton = (ActionButton) findViewById(R.id.action_button);
        actionButton.setImageResource(R.drawable.ic_note_text_black_18dp);
        numberPicker = (NumberPicker) findViewById(R.id.NumberPicker);

        numberPicker.setMinValue(DEFAULT_CHAR_COUNT_MIN);
        numberPicker.setMaxValue(DEFAULT_CHAR_COUNT_MAX);
        numberPicker.setWrapSelectorWheel(false);
        numberPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {

            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                charCount = newVal;
            }
        });

        numberPicker.setValue(DEFAULT_CHAR_COUNT);
    }

    public void SelectFileBrowser(View view) {

        Intent fileIntent = new Intent(Intent.ACTION_GET_CONTENT);
        fileIntent.setType(getString(R.string.MIME_FileSelector));
        try {
            startActivityForResult(fileIntent, 1);
        } catch (ActivityNotFoundException e) {
            toastMessage(getString(R.string.InstallFileExplorer));
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data == null) {
            toastMessage(getString(R.string.SomeThingWentWrong));
            return;
        }
        switch (requestCode) {
            case 1:
                if (resultCode == RESULT_OK) {
                    TextFileInputModel textFileInputModel = new TextFileInputModel(numberPicker.getValue(), data.getData().getPath());

                    if (TxtFileUtil.isTextFile(textFileInputModel.path)) {
                        TextCounterTask textCounterTask = new TextCounterTask(this, findViewById(R.id.RootMenu_RelativeLayout));
                        textCounterTask.execute(textFileInputModel);
                    } else {
                        toastMessage(getString(R.string.SelectTxtFile));
                    }
                }
                break;
            default:
                toastMessage(getString(R.string.SomeThingWentWrong));
                break;
        }
    }

    private void toastMessage(String toastText) {
        Toast.makeText(this, toastText, Toast.LENGTH_SHORT).show();
    }
}
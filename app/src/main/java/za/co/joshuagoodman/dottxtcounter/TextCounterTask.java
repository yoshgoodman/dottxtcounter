package za.co.joshuagoodman.dottxtcounter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class TextCounterTask extends AsyncTask<TextFileInputModel, Integer, String> {

    ProgressDialog progressDialog;
    private View rootView;
    private String outPutString;
    private Context context;

    public TextCounterTask(Context context, View rootView) {
        this.rootView = rootView;
        this.context = context;
    }

    @Override
    protected String doInBackground(TextFileInputModel... params) {
        if (!isCancelled())
            try {
                String path = params[0].path;
                Integer charCount = params[0].charCount;
                outPutString = readText(path, charCount);
                return outPutString;

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        return null;
    }

    @Override
    protected void onPostExecute(String result) {
        progressDialog.dismiss();
        if (isCancelled()) return;
        TextView textViewOutPut = (TextView) rootView.findViewById(R.id.TextOutPut);
        textViewOutPut.setText(outPutString);
    }

    @Override
    protected void onPreExecute() {
        progressDialog = ProgressDialog.show(context,
                getStringFromResources(R.string.ProcessDialogHeading),
                getStringFromResources(R.string.ProcessDialogMessage));
        progressDialog.setCancelable(true);
        progressDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                cancel(true);
                Toast.makeText(context, R.string.CancelledProcessing, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public String readText(String filePath, Integer charCount) throws FileNotFoundException {
        Map<String, Integer> words_count = new HashMap<>();
        FileInputStream fileInputStream = new FileInputStream(filePath);
        Scanner scanner = new Scanner(fileInputStream);
        Integer frequency = null;
        String mostFrequent = null;
        Integer charCountFrequency = null;
        String charCountMostFrequent = null;

        while (scanner.hasNextLine()) {
            if (isCancelled()) return null;
            LineWordSplitter(words_count, scanner);
        }

        for (String wordKey : words_count.keySet()) {
            if (isCancelled()) return null;
            Integer wordCount = words_count.get(wordKey);
            if (frequency == null) {
                frequency = wordCount;
                mostFrequent = wordKey;
            }
            if (wordCount > frequency) {
                frequency = wordCount;
                mostFrequent = wordKey;
            }

            if (wordKey.length() == charCount) {
                if (charCountFrequency == null) {
                    charCountFrequency = wordCount;
                    charCountMostFrequent = wordKey;
                }
                if (wordCount > charCountFrequency) {
                    charCountFrequency = wordCount;
                    charCountMostFrequent = wordKey;
                }
            }
        }
        return outputStringBuilder(charCount, frequency, mostFrequent, charCountFrequency, charCountMostFrequent);
    }

    private void LineWordSplitter(Map<String, Integer> words_count, Scanner scanner) {
        String nextLine = scanner.nextLine();
        String[] roll = nextLine.split("\\s");
        for (String line : roll) {
            if (isCancelled()) return;
            String[] words = line.split("\\s+");
            for (String word : words) {
                if (isCancelled()) return;
                word = word.trim().toLowerCase().replaceAll("\\W", "");
                if (!word.equals("")) {
                    if (words_count.keySet().contains(word)) {
                        Integer count = words_count.get(word) + 1;
                        words_count.put(word, count);
                    } else
                        words_count.put(word, 1);
                }
            }
        }
    }

    @NonNull
    private String outputStringBuilder(Integer charCount, Integer frequency, String mostFrequent, Integer charCountFrequency, String charCountMostFrequent) {
        String output = getStringFromResources(R.string.MostCommonWord) + ": \n";
        if (mostFrequent != null)
            output += "'" + mostFrequent + "'" + getStringFromResources(R.string.Occurred) + frequency + getStringFromResources(R.string.Times);
        else
            output += getStringFromResources(R.string.WordNotFound);

        output += "\n\n" + charCount + getStringFromResources(R.string.chars) + " - " + getStringFromResources(R.string.MostCommonWord) + ": \n";

        if (charCountMostFrequent != null)
            output += "'" + charCountMostFrequent + "'" + getStringFromResources(R.string.Occurred) + charCountFrequency + getStringFromResources(R.string.Times);
        else
            output += getStringFromResources(R.string.WordNotFound);
        return output;
    }

    private String getStringFromResources(Integer id) {
        return context.getResources().getString(id);
    }
}
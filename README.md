### Prerequisites ###
Please have ES File Explorer File Manager Installed on device -  [https://play.google.com/store/apps/details?id=com.estrongs.android.pop&hl=en](Link URL)

### Instructions ###
This project was build in Android Studio:

* compileSdkVersion 21
* buildToolsVersion "22.0.1"
* minSdkVersion 15
* targetSdkVersion 21*

It has been tested on a Samsung Galaxy S5. 

A Gradle Sync, Build and Run should run the app

### Question ###
The responsiveness of a mobile application is one of the priorities in determining acceptable usability. Please write a paragraph describing the performance achieved and possible ways to improve your application’s responsiveness.

### Answer ###
Responsiveness was attempted to be achieved through feedback of the app, telling the user the current status. The user is always able to perform the next action intuitively through the use of icons and placement of elements. The UI thread is not blocked once the file is processing so if the user wishes to cancel the process he can. The user can change the screen orientation and will not loose their session. 

We can improve the application in multiple ways, here are some ideas:

* Feed back in the form of percentage of file processing complete.
* Developing a built in file browser to select the .txt file.
* Being able to open a .txt file with our application and processing it. 
* Giving the user more results and valuable data.

### References ###
[http://stackoverflow.com/questions/18626233/how-do-you-find-words-in-a-text-file-and-print-the-most-frequent-word-shown-usin](Link URL)